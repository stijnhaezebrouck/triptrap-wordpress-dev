<?php
require_once 'modules/tt-albums/date/class-date.php';
require_once 'modules/tt-albums/date/class-schooljaar.php';

use tt_albums\date\Schooljaar;

$connection = mysqli_connect("localhost:3382", "wordpress", "wordpress", "wordpress");

if($connection === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}
$schooljaar = Schooljaar::current();
