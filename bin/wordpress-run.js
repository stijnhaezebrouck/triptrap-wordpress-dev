const dockercompose = require('docker-compose');
const exec  = util.promisify(require('child_process').exec);
const waitPort = require('wait-port');

const pino = require('pino');
const log = pino({ level: process.env.LOG_LEVEL || 'info', prettyPrint: { colorize: true, translateTime: true }});

async function tt_wordpress_exec(args) {
    return exec(`docker exec tt-wordpress ${args}`);
}

async function wp(args) {
    log.info(`wp ${args}`);
    return tt_wordpress_exec(`/bin/wp ${args}`);
}

async function db_ready() {
    await exec("bin\\wait-until-db.sh");
    await waitPort({ host: 'localhost', port: 3382 });
}

async function start_docker() {
    dockercompose.upAll({ cwd: __dirname, log: true }).catch((err) => console.log(err));
}

async function wp_setup() {
    await wp("core install --title=Develop --admin_user=admin --admin_password=admin --admin_email=web@haezebrouck.be --url=http://localhost:8082 --skip-email");
    await tt_wordpress_exec("sudo chown www-data:www-data /var/www/html/wp-content /var/www/html/wp-content/themes");
}

async function data_setup() {
    let currentYear = new Date().getFullYear();
    await exec(`docker exec tt-db mysql -u wordpress -pwordpress -e "INSERT IGNORE INTO wp_tt_albums_album (id, klas, long_title, canonical, short_title, url, hidden, new, pub_status, album_type, album_date) ` +
        `VALUES (1, '1A', 'Stijn', 'stijn', 'Stijn', 'https://www.example.com', 0, 0, 'PUBLISHED', 'BIRTHDAY', '${currentYear}-09-01');" wordpress`)
    return exec(`docker exec tt-db mysql -u wordpress -pwordpress -e "INSERT IGNORE INTO wp_tt_albums_klas (id, klas, relative_url, leerkracht_naam, leerkracht_titel, label) ` +
        `VALUES (10, '1A', '1a', 'Leerkracht', 'juf', '1a');" wordpress`)
}

async function wp_theme_setup() {
    await wp("theme install twentythirteen");
    return wp("theme activate triptrap-2013-child");
    //return wp("theme delete twentytwenty twentynineteen twentyseventeen");
}

async function wp_plugin_setup() {
    tt_wordpress_exec("sudo rm -rf /var/www/html/wp-content/plugins/hello.php /var/www/html/wp-content/plugins/akismet");
    return wp("plugin activate tt-albums tt-thema");
}

async function wp_widget_setup() {
    await wp('widget list "sidebar-2" --format=ids')
        .then(({stdout, stderr}) => {
            log.info(`widgets installed on sidebar-2 are: ` + stdout + stderr);
        }).catch(({stdout, stderr}) => log.error(`failed: wp widget list sidebar-2 --format=ids: `+stdout + stderr));

    //await wp_widget_delete("archives-2");
    //await wp_widget_delete("categories-2");
    //await wp_widget_delete("meta-2");


    let jaar = (new Date()).getFullYear();
    let schooljaar = jaar.toString(10)+'-'+(jaar+1).toString(10).substr(2);
    await wp(`widget add verjaardag "sidebar-2" 1 --aantal="5" --schooljaar="${schooljaar}"`).then(result => console.log(result));
    await wp('widget add "tt-thema" "sidebar-2" 2 --dummy="0"').then(({stdout, stderr}) => console.log(`succeeded: wp widget add "tt-thema" "sidebar-2" 2: `+stdout + stderr))
        .catch(({stdout, stderr}) => console.log(`failed: wp widget add "tt-thema" "sidebar-2" 2: `+stdout + stderr));

}

async function wp_widget_delete(widget) {
    await wp(`widget delete "${widget}"`);
}

async function start() {
    log.info("start");
    await start_docker();
    await db_ready();
    await wp_setup();
    await wp_theme_setup();
    await wp_plugin_setup();
    await data_setup();
    await wp_widget_setup();
}

function end() {
    dockercompose.down({ cwd: __dirname, log: true }).catch((err) => log.error(err));
}

exports.start = start;
exports.end = end;