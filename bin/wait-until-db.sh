#!/usr/bin/env bash

command='docker exec tt-db mysql -u wordpress -pwordpress -e "select 1"'
timeout="30"
i=0
until eval "${command}"
do
    sleep 1
    ((i++))
    echo "attempt $i"

    if [ "${i}" -gt "${timeout}" ]; then
        echo "command was never successful, aborting due to ${timeout}s timeout!"
        exit 1
    fi
done
