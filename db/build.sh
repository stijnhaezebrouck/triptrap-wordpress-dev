#!/bin/sh

version=$1
tagbase=registry.gitlab.com/stijnhaezebrouck/triptrap-wordpress-dev/test-db

echo building version $version

docker build -t $tagbase:$version -t $tagbase:latest $(dirname $0)
docker push $tagbase:$version
docker push $tagbase:latest

