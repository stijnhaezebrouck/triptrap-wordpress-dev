#!/bin/bash
attempt_counter=0
max_attempts=10
port=$1
password=$2
if [ $# -ne 2 ]; then
  echo "expected 2 arguments: port password"
  exit 1
fi

curl --output /dev/null --silent --fail http://localhost:$port/wp-json
rc=$?
if [ $rc -ne 0 ];then
  echo "failed to retrieve wp-json"
  exit 2
fi

curl --output /dev/null --silent --fail http://localhost:$port/wp-json/resttest/unauthenticated
rc=$?
if [ $rc -ne 0 ];then
  echo "failed to execute ttalbums rest test unauthenticated"
  exit 3
fi

header="X-tt-albums-rest-authorization"
passwordb64=$(echo -n $password | base64)
echo test authenticated
curl --output /dev/null --silent -H "$header: $passwordb64" --fail http://localhost:$port/wp-json/resttest/authenticated
rc=$?
if [ $rc -ne 0 ];then
  echo "failed to execute ttalbums rest test authenticated"
  exit 4
fi

echo success!
exit 0

