#!/bin/bash
attempt_counter=0
max_attempts=10
port=$1
echo wait for http :$port

until $(curl --output /dev/null --silent --head --fail http://localhost:$port); do
    if [ ${attempt_counter} -eq ${max_attempts} ];then
      echo "Max attempts ($max_attempts) reached"
      exit 1
    fi

    printf '.'
    attempt_counter=$(($attempt_counter+1))
    sleep 5
done
echo gelukt
exit 0